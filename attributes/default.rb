
default['chef-server']['version'] = "12.0.0-rc.6"
default['chef-server']['prereleases'] = false
default['chef-server']['nightlies'] = false
default['chef-server']['package_file'] = "/vagrant/chef-server-core_12.0.0-rc.6_amd64.deb"
default['chef-server']['package_checksum'] = nil
default['chef-server']['package_options'] = nil
default['chef-server']['api_fqdn'] = node['fqdn']

#default['chef-server']['configuration'] = {}


node['chef_server']['chef-server-webui']['enable'] = true
node['chef_server']['nginx']['enable_non_ssl'] = true
node['chef_server']['nginx']['non_ssl_port'] = 80
node['chef_server']['nginx']['ssl_port'] = 443



#node.override['chef-server']['configuration']['nginx']['ssl_port'] = 4433


# chef-server-core_12.0.0-rc.6-1_amd64.deb


#default['chef-server']['version'] = :latest
#default['chef-server']['prereleases'] = false
#default['chef-server']['nightlies'] = false
#default['chef-server']['package_file'] = nil
#default['chef-server']['package_checksum'] = nil
#default['chef-server']['package_options'] = nil
#default['chef-server']['api_fqdn'] = node['fqdn']

#default['chef-server']['configuration'] = {}
