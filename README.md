arz-chef-server Cookbook
========================

This cookbook installs a chef-server by using chef-server cookbook from supermarket and overriding some attributes.

Requirements
------------

e.g.
#### packages
- `chef-server` - arz-chef-server needs chef-server.

Attributes
----------
TODO: List your cookbook attributes here.

e.g.
#### arz-chef-server::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['arz-chef-server']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

Usage
-----
#### arz-chef-server::default
TODO: Write usage instructions for each cookbook.

e.g.
Just include `arz-chef-server` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[arz-chef-server]"
  ]
}
```

Contributing
------------
TODO: (optional) If this is a public cookbook, detail the process for contributing. If this is a private cookbook, remove this section.

e.g.
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: TODO: List authors
