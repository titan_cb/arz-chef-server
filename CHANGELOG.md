arz-chef-server CHANGELOG
=========================

This file is used to list changes made in each version of the arz-chef-server cookbook.

0.1.0
-----
- Christian Bitschnau - Initial release of arz-chef-server

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
